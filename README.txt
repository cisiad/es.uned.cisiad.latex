This repository contains the next Bibtex files:
- cisiad.bib: General file of references of CISIAD group.
- cochlear.bib: References related to deafness, especially to cochlear implants.
